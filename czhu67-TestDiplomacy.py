
from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_display, diplomacy_print


class test_diplomacy (TestCase) :
    def test_eval_1(self):
        r = "A Madrid Hold"
        w = ""
        o = diplomacy_display(r, w)
        self.assertEqual(o, "A Madrid")
    
    def test_eval2(self):
        r = "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Dublin Support D"
        w = ''
        o = diplomacy_display(r, w)
        self.assertEqual(o, "A [dead]\nB Madrid\nC [dead]\nD Paris\nE Dublin")
        
    def test_eval3(self):
        r = "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Dublin Support D\nF Dallas Move Paris"
        w = ''
        o = diplomacy_display(r, w)
        self.assertEqual(o, "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Dublin\nF [dead]")
        
    def test_eval4(self):
        r = "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A"
        w = ''
        o = diplomacy_display(r, w)
        self.assertEqual(o, "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin")
        
    def test_eval5(self):
        r = "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B"
        w = ''
        o = diplomacy_display(r, w)
        self.assertEqual(o, "A [dead]\nB Madrid\nC London")
        
if __name__ == "__main__":
    main()
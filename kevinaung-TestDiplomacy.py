#!/usr/bin/env python3

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------

class TestDiplomacy (TestCase):
    # ----
    # solve
    # ----

    def test_solve1(self):
        input = StringIO("A Madrid Hold\n")
        output = StringIO()
        diplomacy_solve(input, output)
        self.assertEqual(output.getvalue(), "A Madrid\n")

    def test_solve2(self):
        input = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        output = StringIO()
        diplomacy_solve(input, output)
        self.assertEqual(output.getvalue(), "A [dead]\nB Madrid\nC London\n")
    
    def test_solve3(self):
        input = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        output = StringIO()
        diplomacy_solve(input, output)
        self.assertEqual(output.getvalue(), "A [dead]\nB [dead]\n")
    
    def test_solve4(self):
        input = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        output = StringIO()
        diplomacy_solve(input, output)
        self.assertEqual(output.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve5(self):
        input = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        output = StringIO()
        diplomacy_solve(input, output)
        self.assertEqual(output.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")
    
    def test_solve6(self):
        input = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        output = StringIO()
        diplomacy_solve(input, output)
        self.assertEqual(output.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_solve7(self):
        input = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        output = StringIO()
        diplomacy_solve(input, output)
        self.assertEqual(output.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
